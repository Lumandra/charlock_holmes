GENCCODE_ASSEMBLY_TYPE=-a gcc-darwin
SO=dylib
SOBJ=dylib
A=a
LIBPREFIX=lib
LIB_EXT_ORDER=.49.1.2.dylib
COMPILE=gcc -DU_ATTRIBUTE_DEPRECATED=    -O2 -fPIC -Wall -ansi -pedantic -Wshadow -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -Wno-long-long  -fno-common -c
LIBFLAGS=-I/Users/user/rails_projects/charlock_holmes_bundle_icu/ext/charlock_holmes/dst/include  -dynamic
GENLIB=gcc -dynamiclib -dynamic -O2 -fPIC -Wall -ansi -pedantic -Wshadow -Wpointer-arith -Wmissing-prototypes -Wwrite-strings -Wno-long-long   
LDICUDTFLAGS=
LD_SONAME=-Wl,-compatibility_version -Wl,49 -Wl,-current_version -Wl,49.1.2 -install_name
RPATH_FLAGS=
BIR_LDFLAGS=
AR=ar
ARFLAGS=r
RANLIB=ranlib
INSTALL_CMD=/usr/local/bin/ginstall -c
